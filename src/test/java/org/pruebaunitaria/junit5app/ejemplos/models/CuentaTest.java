package org.pruebaunitaria.junit5app.ejemplos.models;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.pruebaunitaria.junit5app.ejemplos.exceptions.DineroInsuficienteException;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
//PER_CLASS se crea por clases -- se retira
class CuentaTest {

    Cuenta cuenta;
    private TestInfo testInfo ;
    private TestReporter testReporter;

    @BeforeEach
    void initMetodoTest( TestInfo testInfo , TestReporter testReporter) {
        this.cuenta = new Cuenta("Jose", new BigDecimal("1000.12345"));
        this.testInfo = testInfo;
        this.testReporter = testReporter;

        System.out.println("Iniciando el metodo #1.");

        testReporter.publishEntry(" ejecutando : " + testInfo.getDisplayName() + " , metodo : " + testInfo.getTestMethod().orElse(null).getName() + " ," +
                " con las etiquetas  " + testInfo.getTags() );

    }

    @AfterEach
    void tearDown() {
        System.out.println("finalizando el metodo de prueba");

    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("inicializando la clase test");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("finalizando la clase test");
    }

    @Tag("cuenta")
    @Test
    @DisplayName("Probando nombre de la cuenta corriente!!")
    void testNombreCuenta() {
//
//        System.out.println(" ejecutando : " + testInfo.getDisplayName() + " , metodo : " + testInfo.getTestMethod().orElse(null).getName() + " ," +
//                " con las etiquetas  " + testInfo.getTags() );

        testReporter.publishEntry(testInfo.getTags().toString());
        if(testInfo.getTags().contains("cuenta")){
            testReporter.publishEntry("hacer algo con la etiqueta cuenta");
        }

        String esperado = "Jose";
        String actual = cuenta.getPersona();
        //Agregando mensajes de falla en los metodos assertions
        assertNotNull(actual, () -> "La cuenta no puede ser nula");
        assertEquals(esperado, actual, () -> "El nombre de la cuenta no esl que se esperaba : Se esperaba " + esperado + ",sin embargo fue " + actual);//afirmar un valor
        assertTrue(actual.equals("Jose"), () -> "nombre de cuenta esperada debe ser igual a la real");//asertEquals
    }

    @Test
    @DisplayName("Probando el saldo de la cuenta")
    void testSaldoCuenta() {
        assertNotNull(cuenta.getSaldo());
        assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    @DisplayName("Probando la referencia de la cuenta")
    void testReferenciaCuenta() {
        Cuenta cuenta = new Cuenta("Jhon Doe", new BigDecimal("8900.9997"));
        Cuenta cuenta2 = new Cuenta("Jhon Doe", new BigDecimal("8900.9997"));
//        asserNottEquals(cuenta,cuenta2);
        assertEquals(cuenta, cuenta2);
    }

    @Test
    @DisplayName("Probando el tipo de cuenta de debito")
    void testDebitoCuenta() {
        cuenta.debito(new BigDecimal(100));
        assertNotNull(cuenta.getSaldo());
        assertEquals(900, cuenta.getSaldo().intValue());
        assertEquals("900.12345", cuenta.getSaldo().toPlainString());
    }

    @Test
    @DisplayName("Probando el credito cuenta")
    void testCreditoCuenta() {
        cuenta.credito(new BigDecimal(100));
        assertNotNull(cuenta.getSaldo());
        assertEquals(1100, cuenta.getSaldo().intValue());
        assertEquals("1100.12345", cuenta.getSaldo().toPlainString());
    }

    @Test
    @Tag("cuenta")
    @Tag("error")
    @DisplayName("Testeando la excepcion de dinero insuficiente")
    void testDineroInsificienteException() {
        Cuenta cuenta = new Cuenta("Jose", new BigDecimal("1000.123456"));
        Exception exception = assertThrows(DineroInsuficienteException.class, () -> {
            cuenta.debito(new BigDecimal(1500));
        });

        String actual = exception.getMessage();
        String esperado = "Dinero Insuficiente";
        assertEquals(esperado, actual);
    }

    @Tag("cuenta")
    @Tag("banco")
    @Test
    @DisplayName("Probando la transferencia de cuentas")
    void testTransferirDineroCuentas() {
        Cuenta cuenta1 = new Cuenta("Jose", new BigDecimal("3000"));
        Cuenta cuenta2 = new Cuenta("Alfredo", new BigDecimal("1500.8989"));

        Banco banco = new Banco();
        banco.setNombre("Banco del estado");
        assertEquals("1500.8989", cuenta2.getSaldo().toPlainString());
        assertEquals("3000", cuenta1.getSaldo().toPlainString());
    }

    @Test
    //@Disabled //deshabilitar prueba unitaria, indica que hay un metodo dehabilitado por alguna razon
    @DisplayName("Probanndo la relacion de cuentas")
    void testRelacionBancoCuentas() {
        //aseguramos que fallar el metodo
        //fail();

        Cuenta cuenta1 = new Cuenta("Jose", new BigDecimal("2500"));
        Cuenta cuenta2 = new Cuenta("Alfredo", new BigDecimal("1500.8989"));
        Banco banco = new Banco();
        banco.addCuenta(cuenta1);
        banco.addCuenta(cuenta2);
        banco.setNombre("Banco del Estado");
        banco.transferir(cuenta2, cuenta1, new BigDecimal(500));
        assertAll(
                () -> {
                    //instruccion de codigo
                    assertEquals("1000.8989", cuenta2.getSaldo().toPlainString(), () -> "El valor del saldo de la cuenta2 no es el esperado");
                },
                () -> assertEquals("3000", cuenta1.getSaldo().toPlainString())//sin instruccion de codigo
                ,
                () -> assertEquals(2, banco.getCuentas().size()),
                () -> {
                    assertEquals("Banco del Estado", cuenta1.getBanco().getNombre());
                },
                () -> {
                    assertEquals("Alfredo", banco.getCuentas().stream().filter(c -> c.getPersona().equals("Alfredo")).findFirst().get().getPersona());
                },
                () -> {
                    assertTrue(banco.getCuentas().stream().anyMatch(c -> c.getPersona().equals("Alfredo")));
                }
        );
    }

    //DDD


    /*
     * MANEJO DE SISTEMAS OPERATIVOS
     * */
    @Test
    //solo sirve para ejecutar los test en un sistema operativo predetemrinado
    @EnabledOnOs(OS.WINDOWS)
    void testSoloWindows() {

    }


    @Test
    @EnabledOnOs({OS.LINUX, OS.MAC})
    void testSoloLinuxMac() {

    }

    @Test
    //NO SE EJECUITARA EN WINDOWS
    @DisabledOnOs(OS.WINDOWS)
    void testNoWindows() {

    }

    ///

    /*
     * PARA VERSIONES DE JAVA
     * */

    @Test
    @EnabledOnJre(JRE.JAVA_8)
    void soloJDK8() {

    }

    @Test
    @EnabledOnJre(JRE.JAVA_11)
    void soloJDK11() {

    }

    @Test
    @EnabledOnJre(JRE.JAVA_15)
    void soloJDK15() {

    }


    @Test
    @DisabledOnJre(JRE.JAVA_15)
    void testNoJDK15() {

    }

    ///fin pruebas de versiones de JAVA

    @Test
    void imprimirSystemProperties() {
        Properties properties = System.getProperties();
        properties.forEach((k, v) -> System.out.println(k + ":" + v));
    }

    @Test
    @EnabledIfSystemProperty(named = "java.version", matches = "*.11.*")
    void testJavaVersion() {

    }

    @Test
    @DisabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
    void testSolo64() {

    }

    @Test
    @EnabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
    void testNO64() {

    }

    @Test
    @EnabledIfSystemProperty(named = "user.name", matches = "JOSE MENDOZA SANCHEZ")
    void testUserName() {

    }

    @Test
    @EnabledIfSystemProperty(named = "ENV", matches = "dev")
    void testDev() {

    }


    @Test
    void imprimirVariablesAmbiente() {
        Map<String, String> getEnv = System.getenv();
        getEnv.forEach((k, v) -> System.out.println(k + " = " + v));
    }


    @Test
    @EnabledIfEnvironmentVariable(named = "JAVA_HOME", matches = "")
    void testJavaHome() {

    }

    @Test
    @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches = "8")
    void testProcesadores() {

    }

    @Test
    @EnabledIfEnvironmentVariable(named = "ENVIRONMENT", matches = "dev")
    void testEnv() {

    }

    @Test
    @DisabledIfEnvironmentVariable(named = "ENVIRONMENT", matches = "prod")
    void testEnvProdDisabled() {

    }

    /*
     * S21 : Ejecucion de test condicional con ASSUMPTIONS programaticamente
     * */
    @Test
    @DisplayName("testSaldoCuentaDev")
    void testSaldoCuentaDev() {
        boolean esDev = "dev".equals(System.getProperty("ENV"));
        //si esta en otro entorno , no ejecutara los siguiente linas de codigo
        assumeTrue(esDev);
        //en caso cumpla la condicion se ejecuta la siguientes pruebas
        assertNotNull(cuenta.getSaldo());
        assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    @DisplayName("testSaldoCuentaDev2")
    void testSaldoCuentaDev2() {
        boolean esDev = "dev".equals(System.getProperty("ENV"));
        //si esta en otro entorno , no ejecutara los siguiente linas de codigo
        assumingThat(esDev,()->{
            assertNotNull(cuenta.getSaldo());
            assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        });

    }


    /*
    * FIN S:21
    * */




    /*
    * CLASE 22. CLASES DE TEST ANIDADAS USANDO @NESTED
    *
    * */
    @Tag("param")
    @Nested
    class SistemaOperativoTest{
        /*
         * MANEJO DE SISTEMAS OPERATIVOS
         * */
        @Test
        //solo sirve para ejecutar los test en un sistema operativo predetemrinado
        @EnabledOnOs(OS.WINDOWS)
        void testSoloWindows() {

        }


        @Test
        @EnabledOnOs({OS.LINUX, OS.MAC})
        void testSoloLinuxMac() {

        }

        @Test
        //NO SE EJECUITARA EN WINDOWS
        @DisabledOnOs(OS.WINDOWS)
        void testNoWindows() {

        }

    }

    @Nested
    class JavaVersionTest{
        @Test
        @EnabledOnJre(JRE.JAVA_8)
        void soloJDK8() {

        }

        @Test
        @EnabledOnJre(JRE.JAVA_11)
        void soloJDK11() {

        }

        @Test
        @EnabledOnJre(JRE.JAVA_15)
        void soloJDK15() {

        }


        @Test
        @DisabledOnJre(JRE.JAVA_15)
        void testNoJDK15() {

        }
    }

    @Nested
    class SistemPropertiesTest
    {
        @Test
        void imprimirSystemProperties() {
            Properties properties = System.getProperties();
            properties.forEach((k, v) -> System.out.println(k + ":" + v));
        }

        @Test
        @EnabledIfSystemProperty(named = "java.version", matches = "*.11.*")
        void testJavaVersion() {

        }

        @Test
        @DisabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
        void testSolo64() {

        }

        @Test
        @EnabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
        void testNO64() {

        }

        @Test
        @EnabledIfSystemProperty(named = "user.name", matches = "JOSE MENDOZA SANCHEZ")
        void testUserName() {

        }

        @Test
        @EnabledIfSystemProperty(named = "ENV", matches = "dev")
        void testDev() {

        }

    }

    @Tag("variables")
    @Nested
    @DisplayName("probando atributos para variables de ambiente")
    class VariablesDeAmbiente{
        @Test
        @EnabledIfEnvironmentVariable(named = "ENVIRONMENT", matches = "dev")
        void testEnv() {

        }

        @Test
        @DisabledIfEnvironmentVariable(named = "ENVIRONMENT", matches = "prod")
        void testEnvProdDisabled() {

        }

        /*
         * S21 : Ejecucion de test condicional con ASSUMPTIONS programaticamente
         * */
        @Test
        @DisplayName("testSaldoCuentaDev")
        void testSaldoCuentaDev() {
            boolean esDev = "dev".equals(System.getProperty("ENV"));
            //si esta en otro entorno , no ejecutara los siguiente linas de codigo
            assumeTrue(esDev);
            //en caso cumpla la condicion se ejecuta la siguientes pruebas
            assertNotNull(cuenta.getSaldo());
            assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @Test
        @DisplayName("testSaldoCuentaDev2")
        void testSaldoCuentaDev2() {
            boolean esDev = "dev".equals(System.getProperty("ENV"));
            //si esta en otro entorno , no ejecutara los siguiente linas de codigo
            assumingThat(esDev,()->{
                assertNotNull(cuenta.getSaldo());
                assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
                assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
                assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
            });

        }

    }

    /*
    *
    * FIN CLASE 22
    * */


    /*
    * CLASE 23 : Repitiendo pruebas con @RepeatedTest
    *
    * */

    @RepeatedTest(value = 5, name = "{displayName} - Repeticion numero {currentRepetition} del  {totalRepetitions}")
    @DisplayName("Probando debitCuenta Repetir.!")
    void testDebitoCuentaConRepeatedTest(RepetitionInfo info) {
        if(info.getCurrentRepetition()==3){
            System.out.println("estamos en la repeticion :" + info.getCurrentRepetition());
        }
        cuenta.debito(new BigDecimal(100));
        assertNotNull(cuenta.getSaldo());
        assertEquals(900, cuenta.getSaldo().intValue());
        assertEquals("900.12345", cuenta.getSaldo().toPlainString());
    }



    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @ValueSource(strings = {"100","200","300","500","700","1000.12345"})
    @DisplayName("Probando el tipo de cuenta de debito , con parametros")
    void testDebitoCuentaConValueSource(String monto) {
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }

    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @CsvSource({"1,100","2,200","3,300","4,500","5,700","6,1000.12345"})
    @DisplayName("Probando el tipo de cuenta de debito , con parametros")
    void testDebitoCuentaConCsvSource(String index , String  monto) {

        System.out.println(index + " ----- >" + monto);

        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }



    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @CsvFileSource(resources = "/data.csv")
    @DisplayName("Probando el tipo de cuenta de debito , con parametros")
    void testDebitoCuentaConCsvFileSource(String  monto) {
        System.out.println( " ----- >" + monto);
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }


    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @CsvFileSource(resources = "/data2.csv")
    @DisplayName("Probando el tipo de cuenta de debito , con parametros")
    void testDebitoCuentaConCsvFileSource2(String  monto) {
        System.out.println( " ----- >" + monto);
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }



    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @MethodSource("montoList")
    @DisplayName("Probando el tipo de cuenta de debito , con parametros.MethodSource")
    void testDebitoCuentaConMethodSource(String  monto) {
        System.out.println( " ----- >" + monto);
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }

    static List<String> montoList(){
        return Arrays.asList("100","200","300","500","700","1000.12345");
    }



    /**
     * Pruebas parametrizadas con @ParamterizedTest
     *
     * */
    @ParameterizedTest(name = "numero {index} ejecutando con valor {0} {argumentsWithNames}")
    @CsvSource({"200,100,Jhon,Andres","250,200","300,300,Pepe,Pepe","510,500,maria,maria","750,700,Pepa,Pepa","1000.12345,1000.12345,Cata,Cata"})
    @DisplayName("Probando el tipo de cuenta de debito , con parametros")
    void testDebitoCuentaConCsvSource2(String saldo , String  monto , String esperado , String actual) {
        cuenta.setSaldo(new BigDecimal(saldo));
        cuenta.debito(new BigDecimal(monto));
        cuenta.setPersona(actual);
        assertNotNull(cuenta.getSaldo());
        assertNotNull(cuenta.getPersona());
        assertEquals(esperado,actual);
        assumeTrue( cuenta.getSaldo().compareTo(BigDecimal.ZERO)>0);
    }


    /*
    * Etiquetando pruebas unitarias @Tag , ejecutar pruebas de forma selectiva
    * */

    @Nested
    @Tag("timeout")
    class EjemploTimeoutTest{

        @Test
        @Timeout(1)
        void pruebaTimeout() throws InterruptedException {
            TimeUnit.MILLISECONDS.sleep(100);
        }

        @Test
        @Timeout(value = 1000,unit = TimeUnit.MILLISECONDS)
        void pruebaTimeout2() throws InterruptedException {
            TimeUnit.MILLISECONDS.sleep(900);
        }

        @Test
        void testTimeoutAssertions() {
            assertTimeout(Duration.ofMillis(4000),()->{
                TimeUnit.MILLISECONDS.sleep(4000);
            });
        }
    }

}